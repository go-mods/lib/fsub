package fsub

import (
	"os"
	"regexp"
	"strings"
)

// Substitute takes a filepath to a textfile and a regular
// expression, representing the structure of filepaths, as arguments
// substituting any occurences of matches with the regular
// expression inside the textfile with the content of the files the
// matched expressions are referring to. If recursive is set to true
// Substitue will be executed in advance, with the same parameters,
// for each file that is referenced in the textfile.
//
// The regular expression may contain one group to isolate the
// actual filepath within the whole expression. Note that such
// matched paths starting with a forward slash (/) will be treated
// as absolute paths whereas other paths are treated as paths
// relative to the location of the original textfile.
//
// Returns the content from the textfile with substituted filepaths
// as string. In case an error occured the second error return value
// will be unequal to nil.
func Substitute(filepath, expr string, recursive bool) (res string, err error) {
	defer catch(&err)

	regx, err := regexp.Compile(expr)
	throw(err)

	bytes, err := os.ReadFile(filepath)
	throw(err)

	content := string(bytes)
	var folder string

	if strings.Contains(filepath, "/") {
		folder = filepath[0 : strings.LastIndex(filepath, "/")+1]
	} else {
		folder = "./"
	}

	res = regx.ReplaceAllStringFunc(content, func(s string) string {
		m := regx.FindStringSubmatch(s)
		var p string

		if len(m) == 1 {
			p = m[0] // no submatch
		} else {
			p = m[1] // first submatch is path to file
		}

		if p[0] != '/' {
			p = folder + p
		}

		if recursive {
			var text string
			text, err = Substitute(p, expr, recursive)
			throw(err)
			return text
		}

		bytes, err = os.ReadFile(p)
		throw(err)
		return string(bytes)
	})

	return res, err
}

// Helper function to check for existence of errors.
func throw(err error) {
	if err != nil {
		panic(err)
	}
}

// Helper function to handle errors in a deferred context.
func catch(err *error) {
	if r := recover(); r != nil {
		switch e := r.(type) {
		case error:
			*err = e
		default:
			panic(e)
		}
	}
}
